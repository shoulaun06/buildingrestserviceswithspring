package payroll;

import static org.hamcrest.CoreMatchers.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Arrays;

import org.hamcrest.Matcher;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.context.junit.jupiter.SpringExtension;//???

/**
 * How to test the hypermedia-based {@link EmployeeController} with everything
 * else mocked out.
 *
 * @author Greg Turnquist
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(EmployeeController.class)
//@Import({ EmployeeController.class })
public class EmployeeControllerTests {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private EmployeeRepository repository;

	@Test
	public void getShouldFetchAHalDocument() throws Exception {

		given(repository.findAll()).willReturn( //
				Arrays.asList( //
						new Employee(1L, "Frodo Baggins", "ring bearer"), //
						new Employee(2L, "Bilbo Baggins", "burglar")));

		mvc.perform(get("/employees").accept(MediaType.APPLICATION_JSON)) //
				.andDo(print()) //
				.andExpect(status().isOk()) //
				.andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is("Frodo Baggins")))
				.andExpect(jsonPath("$[0].role", is("ring bearer")))
				.andExpect(jsonPath("$[1].id", is(2)))
				.andExpect(jsonPath("$[1].name", is("Bilbo Baggins")))
				.andExpect(jsonPath("$[1].role", is("burglar")))
				.andReturn();
	}

}