package payroll;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long>/* <T, ID> */ {
//http://localhost:8080/h2-console/
}
